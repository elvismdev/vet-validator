<?php
// Include 'Composer' autoloader.

include 'vendor/autoload.php';

// Parse pdf file and build necessary objects.
$parser = new \Smalot\PdfParser\Parser();
$pdf    = $parser->parseFile( 'files/bon.pdf' );

// $text = nl2br( $pdf->getText() );
// echo $text;

$text = $pdf->getText();

if ( strpos( $text, 'Yes' ) !== false ) {
	echo 'IS A VET!';
} else {
	echo 'NOT A VET :(';
}

?>